package net.ddns.ppls.gge_vertretung;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.google.gson.JsonObject;

import net.ddns.ppls.ggeapi.AuthConfig;
import net.ddns.ppls.ggeapi.Credentials;
import net.ddns.ppls.ggeapi.Eintrag;
import net.ddns.ppls.ggeapi.VertretungsTabBase;

import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by Philipp on 18.05.2017.
 */

public class ApiInterface {

    public static final String AUTHAPIBASE = "https://ppluss.de/ggeauth/auth.php";
    public static final String AUTHCONFIGLOCATION = "https://ppluss.de/ggeauth/authconfig.json";
    public static AuthConfig authConfig = null;

    private static String getText(URL url, String charset) throws IOException {
            URL website = url;
            URLConnection conn = website.openConnection();
            conn.setRequestProperty("User-Agent", "GGEApiBot/1.0 PplusSBot/1.0");
            conn.setRequestProperty("Accept", "text/html");
            InputStream in = conn.getInputStream();
            return IOUtils.toString(in, charset);
    }


    public static int[] getKWs() {

        int day = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);

        int week = Calendar.getInstance().get(Calendar.WEEK_OF_YEAR);

        if(day == 1 || day == 7 || day == 6) {
            return new int[]{week, week+1};
        } else {
            return new int[]{week};
        }

    }

    public static URL getURL(int kw) throws IOException {
        String kwstr = kw + "";
        if(kwstr.length() == 1){
            kwstr = "0" + kwstr;
        }
        return new URL("https://www.grashof-gymnasium-bredeney.de/site/vertretungsplan/VPonline/" + kwstr + "/w/w00000.htm");
    }

    public static Eintrag[] getAllEntries(int[] kws) throws IOException {
        if(kws.length == 1) {
            return getEntries(getURL(kws[0]));
        } else {
            ArrayList<Eintrag> eintraege = new ArrayList<>();
            int fails = 0;
            for(int kw: kws) {
                try {
                    eintraege.addAll(Arrays.asList(getEntries(getURL(kw))));
                } catch(Exception e) {
                    e.printStackTrace();
                    fails++;
                }
            }
            if(fails == kws.length) {
                throw new IOException();
            }
            return eintraege.toArray(new Eintrag[0]);
        }
    }

    public static Eintrag[] getEntries(URL url) throws IOException {

            String html = getText(url, "ISO-8859-1");

            Document doc = Jsoup.parse(html);

            Elements tables = doc.getElementsByTag("table");

            ArrayList<Eintrag> entries = new ArrayList<Eintrag>();

            int day = 1;

            String lastmotd = "";

            for(Element table: tables){
                Element tbody = table.getElementsByTag("tbody").get(0);
                Elements trs = tbody.getElementsByTag("tr");
                if(trs.get(0).text().equals("Vertretungen sind nicht freigegeben") || trs.get(0).text().equals("Keine Vertretungen")) {

                } else if(!table.className().contains("subst")){
                    lastmotd = "";
                    for(int i = 1; i < trs.size(); i++) {
                        lastmotd += trs.get(i).getAllElements().get(0).text() + "\n\n";
                    }
                } else {

                    int i = 0;
                    for(Element tr: trs){
                        if(i != 0){
                            Elements tds = tr.getElementsByTag("td");
                            Eintrag eintrag = new Eintrag();
                            eintrag.type = 0;
                            eintrag.datum = tds.get(0).text();
                            if(!lastmotd.equals("")){
                                Eintrag motd = new Eintrag();
                                motd.type = 1;
                                motd.datum = eintrag.datum;
                                motd.text = lastmotd;
                                entries.add(motd);
                                lastmotd = "";
                            }
                            eintrag.stunde = tds.get(1).text();
                            eintrag.klasse = tds.get(2).text();
                            eintrag.fach = tds.get(3).text();
                            eintrag.raum_ = tds.get(4).text();
                            eintrag.raum = tds.get(5).text();
                            eintrag.vertreter = tds.get(6).text();
                            eintrag.text = tds.get(7).text();
                            entries.add(eintrag);
                        }
                        i++;
                    }
                }
                day++;
            }

            if(!lastmotd.equals("")) {
                Eintrag motd = new Eintrag();
                motd.type = 1;
                motd.datum = "???";
                motd.text = lastmotd;
                entries.add(motd);
                lastmotd = "";
            }

            return entries.toArray(new Eintrag[0]);

    }

    public static Eintrag[] filter(Eintrag[] raw, Context context) {
        String[] aktivierteKlassen = Storage.instance.getEnabledClasses(context);
        if(aktivierteKlassen.length == 0) {
            return raw;
        } else {
            ArrayList<Eintrag> include = new ArrayList<>();
            for(Eintrag eintrag : raw) {
                if(eintrag.type == 0) {
                    for(String klasse : aktivierteKlassen) {
                        if(eintrag.klasse.toUpperCase().contains(klasse.toUpperCase())) {
                            include.add(eintrag);
                            break;
                        } else if(eintrag.klasse.toUpperCase().equals(klasse.substring(0,1).toUpperCase())) {
                            include.add(eintrag);
                            break;
                        }
                    }
                } else {
                    include.add(eintrag);
                }
            }
            return include.toArray(new Eintrag[0]);
        }
    }

    public static ArrayList<VertretungsTabBase> getTabBases(ArrayList<Eintrag> vertretungen, Context context) {
        ArrayList<String> dates = new ArrayList<>();
        for(Eintrag vertretung: vertretungen) {
            if(vertretung.type == 2) {
                if(!dates.contains(context.getString(R.string.tips))) {
                    dates.add(context.getString(R.string.tips));
                }
            } else if(!dates.contains(vertretung.datum)) {
                dates.add(vertretung.datum);
            }
        }
        ArrayList<VertretungsTabBase> tabBases = new ArrayList<>();
        for(String date: dates) {
            VertretungsTabBase base = new VertretungsTabBase();
            base.eintraege = new ArrayList<>();
            base.name = date;
            tabBases.add(base);
        }
        for(Eintrag vertretung: vertretungen) {
            for(VertretungsTabBase tabBase: tabBases) {
                if(vertretung.type == 2 && context.getString(R.string.tips).equals(tabBase.name)) {
                    tabBase.eintraege.add(vertretung);
                } else if(vertretung.datum.equals(tabBase.name)) {
                    tabBase.eintraege.add(vertretung);
                }
            }
        }
        return tabBases;
    }

    public static boolean authorize(Context context) throws Exception {

            if(authConfig == null) {
                authConfig = new Gson().fromJson(getText(new URL(AUTHCONFIGLOCATION), "UTF-8"), AuthConfig.class);
            }

            Credentials cred = Storage.instance.getCredentials(context);
            if(cred.username == null) {
                String text = getText(new URL(AUTHAPIBASE + "?useauth=false"), "UTF-8");
                JsonObject jo = new JsonParser().parse(text).getAsJsonObject();
                return jo.get("success").getAsBoolean();
            }

            String encuser = URLEncoder.encode(cred.username, "UTF-8");
            String encpassword = URLEncoder.encode(cred.password, "UTF-8");

            String text = getText(new URL(AUTHAPIBASE + "?username=" + encuser + "&password=" + encpassword), "UTF-8");
            JsonObject jo = new JsonParser().parse(text).getAsJsonObject();
            boolean res = jo.get("success").getAsBoolean();

            if(!res) {
                if(context instanceof Activity) {
                    final Activity a = (Activity) context;
                    a.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(a, authConfig.invalid, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }

            return res;

    }

    public static void showLoginDialog(final MainActivity context) throws Exception {

        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    LayoutInflater factory = LayoutInflater.from(context);
                    final View textEntryView = factory.inflate(R.layout.login, null);
                    AlertDialog.Builder alert = new AlertDialog.Builder(context);
                    alert.setTitle(authConfig.title);
                    alert.setMessage(authConfig.text);
                    alert.setView(textEntryView);
                    alert.setCancelable(false);
                    ((TextView)textEntryView.findViewById(R.id.userNameTextView)).setText(authConfig.username);
                    ((TextView)textEntryView.findViewById(R.id.passwordTextView)).setText(authConfig.password);
                    alert.setPositiveButton(authConfig.loginbtn, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {

                            final EditText usernameInput = (EditText) textEntryView.findViewById(R.id.userNameEditText);
                            final EditText passwordInput = (EditText) textEntryView.findViewById(R.id.passwordEditText);

                            String username = usernameInput.getText().toString();
                            String password = passwordInput.getText().toString();

                            Storage.instance.setCredentials(username, password, context);
                            context.refresh(false);

                        }
                    });
                    alert.show();
                } catch(Exception e) {
                    e.printStackTrace();
                }

            }
        });

    }

}
