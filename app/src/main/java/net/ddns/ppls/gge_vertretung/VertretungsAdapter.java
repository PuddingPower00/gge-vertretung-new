package net.ddns.ppls.gge_vertretung;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.CycleInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import net.cachapa.expandablelayout.ExpandableLayout;
import net.cachapa.expandablelayout.util.FastOutSlowInInterpolator;
import net.ddns.ppls.ggeapi.Eintrag;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

public class VertretungsAdapter extends RecyclerView.Adapter<VertretungsAdapter.VertretungsViewHolder> {
    Eintrag[] mDataset;
    boolean[] expandedList;

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public static class VertretungsViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final TextView textview2, textView;
        final ExpandableLayout expandableLayout;
        final Button actionbtn;
        VertretungsAdapter adapter;
        Eintrag mEintrag;
        public VertretungsViewHolder(View v) {
            super(v);
            mView = v;
            textview2 = v.findViewById(R.id.textView2);
            textView = v.findViewById(R.id.textView);
            expandableLayout = v.findViewById(R.id.expandable_layout);
            actionbtn = v.findViewById(R.id.actionbtn);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Log.println(Log.WARN, "listener", "Entry " + getAdapterPosition() + " clicked!");
                    //showSubitutionAlert(view.getContext(), latestDataset[getAdapterPosition()]);
                    ExpandableLayout expandableLayout = view.findViewById(R.id.expandable_layout);
                    expandableLayout.setExpanded(!expandableLayout.isExpanded());
                    VertretungsViewHolder.this.adapter.expandedList[getAdapterPosition()] = expandableLayout.isExpanded();
                }
            });
            v.findViewById(R.id.actionbtn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    Eintrag eintrag = adapter.mDataset[getAdapterPosition()];
                    if(eintrag.type == 2 && eintrag.vertreter.equals("classlist")) {
                        Activity activity = getActivity(view.getContext());
                        if(activity instanceof MainActivity) {
                            ((MainActivity)activity).showClassSwitcher();
                        }
                    } else if(eintrag.type == 2) {

                        Activity activity = getActivity(view.getContext());
                        if(activity instanceof MainActivity) {
                            Storage.instance.dismissMessage(eintrag.fach, activity);
                            ((MainActivity)activity).swipeRefreshLayout.setRefreshing(true);
                            ((MainActivity)activity).refresh(true);
                            // TELEGRAM KANAL JOIN

                            if(eintrag.fach.equals("telegram")) {
                                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if(which == DialogInterface.BUTTON_POSITIVE) {
                                            Intent telegramJoin = new Intent(Intent.ACTION_VIEW, Uri.parse("https://t.me/ggevertretung"));
                                            view.getContext().startActivity(telegramJoin);
                                        }
                                    }
                                };

                                AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                                builder.setMessage("Möchtest du dem Telegram-Kanal beitreten?").setPositiveButton("Ja", dialogClickListener)
                                        .setNegativeButton("Nein", dialogClickListener).show();
                            }

                        }

                    } else {
                        //Toast.makeText(view.getContext(), "noch nicht implementiert lol", Toast.LENGTH_SHORT).show();
                        Sharing.showShareDialog(view.getContext(), eintrag);
                    }
                }
            });
        }
    }

    public VertretungsAdapter(Eintrag[] myDataset) {
        mDataset = myDataset;
        expandedList = new boolean[mDataset.length];
    }

    @Override
    public VertretungsAdapter.VertretungsViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        View v =  (View) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.vertretung, parent, false);

        VertretungsViewHolder vh = new VertretungsViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(VertretungsViewHolder holder, int position) {
        final Eintrag eintrag = mDataset[position];
        holder.mEintrag = mDataset[position];
        holder.adapter = VertretungsAdapter.this;
        String str = "";
        if(eintrag.type == 0) {
            str = holder.mView.getContext().getString(R.string.klasse) + " " + eintrag.klasse + ": " + eintrag.stunde + ". " + holder.mView.getContext().getString(R.string.lesson) + " (" + eintrag.fach + ")";
            holder.actionbtn.setText(R.string.sharetitle);
        } else if(eintrag.type == 1) {
            str = holder.mView.getContext().getString(R.string.motd);
            holder.actionbtn.setText(R.string.sharetitle);
        } else if(eintrag.type == 2 && eintrag.vertreter.equals("classlist")) {
            str = eintrag.datum;
            holder.actionbtn.setText(R.string.activate_class);
        } else if(eintrag.type == 2) {
            str = eintrag.datum;
            holder.actionbtn.setText(R.string.ok);
        }

        holder.textView.setText(str);

        String msg = "", title = "";
        if(eintrag.type == 0) {
            msg = holder.mView.getContext().getString(R.string.date) + ": " + eintrag.datum + "\n" + holder.mView.getContext().getString(R.string.lesson) + ": " + eintrag.stunde + "\n" + holder.mView.getContext().getString(R.string.subject) + ": " + eintrag.fach + "\n" + holder.mView.getContext().getString(R.string.klasse) + ": " + eintrag.klasse + "\n" + holder.mView.getContext().getString(R.string.room) + ": " + eintrag.raum + "\n" + holder.mView.getContext().getString(R.string.room_) + ": " + eintrag.raum_ + "\n" + holder.mView.getContext().getString(R.string.substitute) + ": " + eintrag.vertreter + "\n" + holder.mView.getContext().getString(R.string.text) + ": " + eintrag.text;
        } else if(eintrag.type == 1) {
            msg = eintrag.text;
        } else if(eintrag.type == 2) {
            msg = eintrag.text;
        }

        holder.textview2.setText(msg);

        if(eintrag.type == 0) {
            if (eintrag.text.toLowerCase().contains("vertretung")) {
                holder.mView.setBackgroundColor(holder.mView.getContext().getResources().getColor(R.color.vertretung));
            } else if(eintrag.text.toLowerCase().contains("sol")) {
                holder.mView.setBackgroundColor(holder.mView.getContext().getResources().getColor(R.color.sol_));
            } else if(eintrag.text.toLowerCase().contains("raumänderung")) {
                holder.mView.setBackgroundColor(holder.mView.getContext().getResources().getColor(R.color.raumaenderung));
            }
        }

        holder.expandableLayout.setInterpolator(new FastOutSlowInInterpolator());

    }

    @Override
    public int getItemCount() {
        return mDataset.length;
    }

    static AlertDialog alertdialog = null;

    private static Activity getActivity(Context context) {
        while (context instanceof ContextWrapper) {
            if (context instanceof Activity) {
                return (Activity)context;
            }
            context = ((ContextWrapper)context).getBaseContext();
        }
        return null;
    }

}