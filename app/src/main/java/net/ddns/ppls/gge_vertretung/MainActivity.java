package net.ddns.ppls.gge_vertretung;

import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.StrictMode;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TabHost;
import android.widget.Toast;

import com.google.android.gms.common.api.Api;

import net.ddns.ppls.ggeapi.Eintrag;
import net.ddns.ppls.ggeapi.VertretungsTabBase;

import java.io.IOException;
import java.lang.reflect.Array;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    boolean initialReload = true;
    private ProgressDialog progressDialog;
    SwipeRefreshLayout swipeRefreshLayout;
    private TabHost tabhost;
    private Eintrag[] raweintraege = null;
    private static String selectedTab = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        setSupportActionBar((Toolbar)findViewById(R.id.my_toolbar));

        swipeRefreshLayout = findViewById(R.id.swiperefresh);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh(false);
            }
        });

        //if(Storage.instance.isNightmodeEnabled(MainActivity.this)) {
        //    setTheme(R.style.AppThemeDark);
        //}

        tabhost = findViewById(R.id.tabhost);
        tabhost.setup();

        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setMessage(getString(R.string.loading_text));
        progressDialog.setTitle(getString(R.string.app_name));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        progressDialog.setCancelable(false);

        refresh(false);

    }

     public void refresh(final boolean fromCache) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if(!ApiInterface.authorize(MainActivity.this)) {
                        if(initialReload) {
                            progressDialog.dismiss();
                            ApiInterface.showLoginDialog(MainActivity.this);
                        }
                        return;
                    }
                    if(!fromCache || raweintraege == null) {
                        raweintraege = ApiInterface.getAllEntries(ApiInterface.getKWs());
                    }
                    ArrayList<Eintrag> eintraege = new ArrayList<>(Arrays.asList(ApiInterface.filter(raweintraege, MainActivity.this)));
                    if(Storage.instance.getEnabledClasses(MainActivity.this).length == 0) {
                        Eintrag notice = new Eintrag();
                        notice.type = 2;
                        notice.datum = getString(R.string.tip_no_classes_activated_title);
                        notice.text = getString(R.string.tip_classfilter);
                        notice.vertreter = "classlist";
                        eintraege.add(0, notice);
                    }
                    if(eintraege.size() == 0) {
                        Eintrag notice = new Eintrag();
                        notice.type = 2;
                        notice.datum = getString(R.string.no_substitutions_title);
                        notice.text = getString(R.string.no_substitutions_text);
                        notice.vertreter = "classlist";
                        eintraege.add(0, notice);
                    }
                    if(!Storage.instance.isMessageDismissed("vernotice5", MainActivity.this)) {
                        Eintrag notice = new Eintrag();
                        //TYPE 2 = CUSTOM
                        notice.type = 2;
                        //DATUM bei TYPE 2 = NACHRICHTENTITEL
                        notice.datum = getString(R.string.changelog_title);
                        //TEXT bei TYPE 2 = TEXT
                        notice.text = getString(R.string.changelog_text);
                        //VERTRETER bei TYPE 2 = AKTIONSBUTTON
                        notice.vertreter = "dismiss";
                        //FACH bei TYPE 2 = ID
                        notice.fach = "vernotice5";
                        eintraege.add(0, notice);
                    }
                    if(!Storage.instance.isMessageDismissed("telegram", MainActivity.this)) {
                        Eintrag notice = new Eintrag();
                        //TYPE 2 = CUSTOM
                        notice.type = 2;
                        //DATUM bei TYPE 2 = NACHRICHTENTITEL
                        notice.datum = "Telegram-Kanal";
                        //TEXT bei TYPE 2 = TEXT
                        notice.text = "Es gibt jetzt einen Telegram-Kanal mit Umfragen und Neuigkeiten über Features. Die Mitgliedschaft in diesem Kanal ist selbstverständlich absolut freiwillig.\n\nTippe auf \"Okay\" um diese Nachricht zu entfernen und/oder dem Kanal beizutreten.";
                        //VERTRETER bei TYPE 2 = AKTIONSBUTTON
                        notice.vertreter = "dismiss";
                        //FACH bei TYPE 2 = ID
                        notice.fach = "telegram";
                        eintraege.add(0, notice);
                    }
                    final ArrayList<VertretungsTabBase> tabBases = ApiInterface.getTabBases(eintraege, MainActivity.this);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            tabhost.setOnTabChangedListener(null);
                            tabhost.clearAllTabs();

                            for(final VertretungsTabBase tabBase: tabBases) {
                                TabHost.TabSpec spec = tabhost.newTabSpec(tabBase.name);
                                spec.setIndicator(tabBase.name);
                                spec.setContent(new TabHost.TabContentFactory() {
                                    @Override
                                    public View createTabContent(String tag) {
                                        View view = LayoutInflater.from(MainActivity.this).inflate(R.layout.vertretungs_tab, null);
                                        RecyclerView recyclerView = view.findViewById(R.id.recycler);
                                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
                                        recyclerView.setLayoutManager(layoutManager);
                                        VertretungsAdapter adapter = new VertretungsAdapter(tabBase.eintraege.toArray(new Eintrag[0]));
                                        recyclerView.setAdapter(adapter);
                                        return view;
                                    }
                                });
                                tabhost.addTab(spec);
                                if(tabBase.name.equals(selectedTab)) {
                                    tabhost.setCurrentTabByTag(tabBase.name);
                                }
                            }

                            tabhost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
                                @Override
                                public void onTabChanged(String tabId) {
                                    selectedTab = tabId;
                                }
                            });

                            //Log.e("testingpps", "" + tabBases.size());

                            if(initialReload) {
                                if((progressDialog != null) && progressDialog.isShowing() ) {
                                    progressDialog.dismiss();
                                }
                                initialReload = false;
                            } else {
                                swipeRefreshLayout.setRefreshing(false);
                            }



                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    if(initialReload) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                builder.setTitle(getString(R.string.app_name));
                                builder.setMessage(getString(R.string.loading_failed));
                                builder.setCancelable(false);
                                builder.setPositiveButton(getString(R.string.retry), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        recreate();
                                    }
                                });
                                builder.show();
                            }
                        });
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                swipeRefreshLayout.setRefreshing(false);
                                Toast.makeText(getApplicationContext(), getString(R.string.loading_failed), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            }
        }).start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        //if(Storage.instance.isNightmodeEnabled(MainActivity.this)) {
        //    menu.findItem(R.id.nightmode).setChecked(true);
        //}

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.changeclass:
                showClassSwitcher();
                return true;
            //case R.id.nightmode:
            //    Storage.instance.setNightmodeEnabled(!item.isChecked(), MainActivity.this);
            //    item.setChecked(!item.isChecked());
            //    setTheme(R.style.AppThemeDark);
            //    return true;
            case R.id.reshowtips:
                Storage.instance.reshowAllMessages(this);
                swipeRefreshLayout.setRefreshing(true);
                refresh(true);
                return true;
            case R.id.share_selection:
                shareSelection();
                return true;
            //case R.id.clear_selection:
            //    clearSelection();
            //    return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void showClassSwitcher() {

        List<String> enabledclasses = Arrays.asList(Storage.instance.getEnabledClasses(MainActivity.this));
        final ArrayList<Integer> mSelectedItems = new ArrayList<>();
        final boolean[] selection = new boolean[Storage.classlist.length];

        for(int i = 0; i < Storage.classlist.length; i++) {
            selection[i] = enabledclasses.contains(Storage.classlist[i]);
            if(selection[i]) {
                mSelectedItems.add(i);
            }
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(getString(R.string.classchooser_title))
            .setMultiChoiceItems(Storage.classlist, selection,
                new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        if (isChecked) {
                            mSelectedItems.add(which);
                        } else if (mSelectedItems.contains(which)) {
                            mSelectedItems.remove(Integer.valueOf(which));
                        }
                    }
                })
            .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    ArrayList<String> classesList = new ArrayList<>();
                    for(int cur : mSelectedItems) {
                        classesList.add(Storage.classlist[cur]);
                    }
                    Storage.instance.setEnabledClasses(classesList.toArray(new String[0]), MainActivity.this);
                    swipeRefreshLayout.setRefreshing(true);
                    refresh(true);
                }
            })
            .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                   dialog.dismiss();
                }
            });
        builder.show();
    }

    public ArrayList<Eintrag> getSelectedItems() {
        View view = tabhost.getCurrentView();
        if(view == null) {
            Log.w("tabhostsoos", tabhost.getCurrentTab()+"");
            return null;
        }
        RecyclerView recyclerView = view.findViewById(R.id.recycler);
        ArrayList<Eintrag> eintraege = new ArrayList<>();
        if(recyclerView.getLayoutManager() == null) {
            return null;
        }
        VertretungsAdapter adapter = (VertretungsAdapter) recyclerView.getAdapter();
        boolean[] expandedList = adapter.expandedList;
        for(int i = 0; i < recyclerView.getAdapter().getItemCount(); i++) {
            if(expandedList[i]) {
                eintraege.add(adapter.mDataset[i]);
            }
        }
        return eintraege;
    }

    /*public void clearSelection() {
        ArrayList<Eintrag> sel = getSelectedItems();
        if(sel == null) {
            return;
        }
        for(VertretungsAdapter.VertretungsViewHolder item : sel) {
            item.expandableLayout.collapse();
        }
    }*/

    public void shareSelection() {
        Toast.makeText(MainActivity.this, "WARNUNG: Das Teilen von mehreren Dateien aufeinmal wird nur von wenigen Apps unterstützt. Evtl. wird das Teilen also fehlschlagen.", Toast.LENGTH_LONG).show();
        ArrayList<Eintrag> sel = getSelectedItems();
        if(sel == null) {
            return;
        }
        ArrayList<Eintrag> eintraege = new ArrayList<>();
        for(Eintrag item : sel) {
            if(item != null) {
                eintraege.add(item);
            }
        }
        Sharing.showMultiShareDialog(MainActivity.this, eintraege);
    }

    /*public static String getDateName(String datestr) {
        //TODO
        SimpleDateFormat parser = new SimpleDateFormat("d.M.");
        java.util.Date date;
        try {
            date = parser.parse(datestr);
        } catch (ParseException e) {
            e.printStackTrace();
            return datestr;
        }

        if(DateUtils.isToday(date.getTime())) {
            return "Heute";
        } else if(DateUtils.isToday(date.getTime() - 86400000L)) {
            return "Morgen";
        } else if(DateUtils.isToday(date.getTime() + 86400000L)) {
            return "Gestern";
        } else {
            String[] tageAufDeutsch = new String[]{"Sonntag","Montag","Dienstag","Mittwoch","Donnerstag","Freitag","Samstag"};
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            return tageAufDeutsch[calendar.get(Calendar.DAY_OF_WEEK)-1];
        }
    }*/

}
