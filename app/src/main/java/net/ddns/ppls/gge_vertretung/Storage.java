package net.ddns.ppls.gge_vertretung;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import net.ddns.ppls.ggeapi.Credentials;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class Storage {

    public static Storage instance = new Storage();

    public static final String[] classlist = new String[]{"5A","5B","5C","6A","6B","6C","7A","7B","7C","8A","8B","8C","9A","9B","9C","10","10GR","11","11GR","Q1GR","Q2","Q2GR","Q1","Q2SE","12","12GRGO","12GR","01SE"};

    public String[] getEnabledClasses(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
        Collection coll = sharedPref.getStringSet("enabledClasses", new HashSet<String>(0));
        return (String[]) coll.toArray(new String[0]);
    }

    public void setEnabledClasses(String[] classes, Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putStringSet("enabledClasses", new HashSet<String>(Arrays.asList(classes)));
        editor.apply();
    }

    public void setNightmodeEnabled(boolean enabled, Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean("nightmode_enabled", enabled);
        editor.apply();
    }

    public boolean isNightmodeEnabled(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
        return sharedPref.getBoolean("nightmode_enabled", false);
    }

    public void setNotificationTime(long time, Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putLong("notifications", time);
        editor.apply();
    }

    public long getNotificationTime(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
        return sharedPref.getLong("notifications", -1L);
    }

    public void setCredentials(String username, String password, Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        Credentials credentials = new Credentials();
        credentials.username = username;
        credentials.password = password;
        Gson gson = new Gson();
        editor.putString("credentials", gson.toJson(credentials));
        editor.apply();
    }

    public Credentials getCredentials(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
        String str = sharedPref.getString("credentials", "{}");
        return new Gson().fromJson(str, Credentials.class);
    }

    public boolean isMessageDismissed(String id, Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
        Set<String> set = sharedPref.getStringSet("dismissed_messages", new HashSet<String>());
        for(String str: set) {
            if(id.equals(str)) {
                return true;
            }
        }
        return false;
    }

    public void dismissMessage(String id, Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        Set<String> set = sharedPref.getStringSet("dismissed_messages", new HashSet<String>());
        ArrayList<String> arrayList = new ArrayList<String>(Arrays.asList(set.toArray(new String[0])));
        arrayList.add(id);
        set = new HashSet<>(arrayList);
        editor.putStringSet("dismissed_messages", set);
        editor.apply();
    }

    public void reshowAllMessages(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putStringSet("dismissed_messages", new HashSet<String>());
        editor.apply();
    }

}
