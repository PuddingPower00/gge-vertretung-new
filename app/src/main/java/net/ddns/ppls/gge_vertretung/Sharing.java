package net.ddns.ppls.gge_vertretung;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v4.content.FileProvider;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.Log;
import android.widget.Toast;

import net.ddns.ppls.ggeapi.Eintrag;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Random;

public class Sharing {
    public static void showShareDialog(Context context, Eintrag eintrag) {
        try {
            cleanShareFiles(context);
            Uri imageUri = genImageUrifromEintrag(eintrag, context);
            Intent shareintent = new Intent(Intent.ACTION_SEND);
            shareintent.setType("image/png");
            shareintent.putExtra(Intent.EXTRA_TEXT, context.getString(R.string.sharemsg));
            shareintent.putExtra(Intent.EXTRA_STREAM, imageUri);
            shareintent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            context.startActivity(Intent.createChooser(shareintent, context.getString(R.string.sharetitle)));
        } catch (Exception e){
            e.printStackTrace();
            Toast.makeText(context, context.getString(R.string.err_share) + ": " + e.getClass().getName(), Toast.LENGTH_SHORT).show();
        }

    }

    public static void showMultiShareDialog(Context context, ArrayList<Eintrag> eintraege) {
        try {
            cleanShareFiles(context);
            ArrayList<Uri> uris = new ArrayList<>();
            ArrayList<String> sharemsgs = new ArrayList<>();
            for(Eintrag eintrag : eintraege) {
                if(eintrag.type != 1 && eintrag.type != 0) {
                    Toast.makeText(context, context.getString(R.string.selection_contains_unshareable), Toast.LENGTH_SHORT).show();
                    return;
                }
                uris.add(genImageUrifromEintrag(eintrag, context));
                sharemsgs.add(context.getString(R.string.sharemsg));
            }
            if(uris.size() == 0) {
                Toast.makeText(context, context.getString(R.string.selection_empty), Toast.LENGTH_SHORT).show();
                shareApp(context);
                return;
            }
            /*Intent viewintent = new Intent(Intent.ACTION_VIEW);
            viewintent.setDataAndType(uris.get(0), "image/png");
            viewintent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            context.startActivity(Intent.createChooser(viewintent, "test"));*/
            Intent shareintent = new Intent(Intent.ACTION_SEND_MULTIPLE);
            shareintent.setType("image/png");
            shareintent.putExtra(Intent.EXTRA_TEXT, sharemsgs);
            shareintent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
            shareintent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            context.startActivity(Intent.createChooser(shareintent, context.getString(R.string.sharetitle)));
        } catch (Exception e){
            e.printStackTrace();
            Toast.makeText(context, context.getString(R.string.err_share) + ": " + e.getClass().getName(), Toast.LENGTH_SHORT).show();
        }
    }

    private static void shareApp(Context context) {
        try {
            Intent shareintent = new Intent(Intent.ACTION_SEND);
            shareintent.setType("text/plain");
            shareintent.putExtra(Intent.EXTRA_TEXT, context.getString(R.string.shareappmsg));
            context.startActivity(Intent.createChooser(shareintent, context.getString(R.string.sharetitle)));
        } catch (Exception e){
            e.printStackTrace();
            Toast.makeText(context, context.getString(R.string.err_share) + ": " + e.getClass().getName(), Toast.LENGTH_SHORT).show();
        }
    }

    private static void cleanShareFiles(Context context) {
        File sharefolder = new File(context.getExternalCacheDir().getAbsolutePath() + "/shareimgs/");
        if(!sharefolder.exists()) {
            sharefolder.mkdirs();
        }
        for(File file : sharefolder.listFiles()) {
            if(file.isFile()) {
                try {
                    file.delete();
                } catch(Exception e) {

                }
            }
        }
    }

    private static Uri genImageUrifromEintrag(Eintrag eintrag, Context context) throws Exception {
        Bitmap bitmap = Bitmap.createBitmap(512, 512, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        TextPaint paint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
        Bitmap template = BitmapFactory.decodeResource(context.getResources(), R.drawable.ggevertretung_share_template);
        template = Bitmap.createScaledBitmap(template, 512, 512, true);
        canvas.drawBitmap(template, 0, 0, paint);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.WHITE);
        paint.setTextSize(28);
        paint.setTypeface(Typeface.create("Roboto",Typeface.NORMAL));
        if(eintrag.type == 0){
            int height = 80, heightdiff = 30;
            canvas.drawText(context.getString(R.string.date) + ": " + eintrag.datum, 80, height, paint);
            height+=heightdiff;
            canvas.drawText(context.getString(R.string.lesson) + ": " + eintrag.stunde, 80, height, paint);
            height+=heightdiff;
            canvas.drawText(context.getString(R.string.subject) + ": " + eintrag.fach, 80, height, paint);
            height+=heightdiff;
            canvas.drawText(context.getString(R.string.room) + ": " + eintrag.raum, 80, height, paint);
            height+=heightdiff;
            canvas.drawText(context.getString(R.string.room_) + ": " + eintrag.raum_, 80, height, paint);
            height+=heightdiff;
            canvas.drawText(context.getString(R.string.klasse) + ": " + eintrag.klasse, 80, height, paint);
            height+=heightdiff;
            canvas.drawText(context.getString(R.string.substitute) + ": " + eintrag.vertreter, 80, height, paint);
            height+=heightdiff;
            canvas.drawText(context.getString(R.string.text) + ": " + eintrag.text, 80, height, paint);
        } else if(eintrag.type == 1){
            canvas.drawText(context.getString(R.string.motd) + " " + eintrag.datum + ": ", 80, 80, paint);

            int textWidth = canvas.getWidth() - 32;
            StaticLayout textLayout = new StaticLayout(eintrag.text, paint, textWidth, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
            canvas.save();
            canvas.translate(16,100);
            textLayout.draw(canvas);
            canvas.restore();
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 10, baos);
        File sharefolder = new File(context.getExternalCacheDir().getAbsolutePath() + "/shareimgs/");
        if(!sharefolder.exists()) {
            sharefolder.mkdir();
        }
        File sharefile = new File(sharefolder.getAbsolutePath() + "share_" + (new Random().nextInt(899999)+100000) + ".png");
        if(sharefile.exists()){
            sharefile.delete();
        }
        FileOutputStream fileout = new FileOutputStream(sharefile);
        fileout.write(baos.toByteArray());
        fileout.close();
        baos.close();
        sharefile.setWritable(true, true);
        sharefile.setReadable(true, false);
        Uri photoURI = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".GenericFileProvider", sharefile);
        return photoURI;
    }
}
